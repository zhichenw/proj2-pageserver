from flask import Flask, abort, render_template

app = Flask(__name__)

@app.route("/<path:path>")
def serve_file(path):
    # check if starts with ~/.
    url = path
    if len(url) != 0:
        url = "/" + path
        for i in range(len(url)-1):
            if url[i] == "/" and url[i + 1] in "~/.":
                abort(403)

    import os
    abspath = os.path.abspath(__file__)
    dirname = os.path.dirname(abspath)
    os.chdir(dirname)
    filepath = "./templates/" + path
    if not os.path.exists(filepath):
        abort(404)
    
    return render_template(path), 200

@app.errorhandler(404)
def error_404(error):
    return render_template("404.html"), 404

@app.errorhandler(403)
def error_404(error):
    return render_template("403.html"), 403

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0', port=5000)
